﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

OBJECT POOLER SCRIPT
    * spawns numbers of prototype instances and then returns one if an instance is requested.
    * object poolers lessen runtime memory allocation
    * note: spawning more than object max instances will just recycle the container
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using System;
using System.Collections.Generic;

public sealed class ObjectPooler : ScriptableObject
{
    private sealed class ObjectPoolPayload
    {
        private uint                maxContainerCount = 20; // max instances for each prototype(default: 20)
        private GameObject          prototype; // game object to clone
        private List<GameObject>    container = new List<GameObject>(); // instances container
        private int                 containerIndex = 0;

        public ObjectPoolPayload(GameObject Prototype)
        {
            prototype = Prototype;
        }

        public ObjectPoolPayload(GameObject Prototype, uint MaxCount)
        {
            maxContainerCount = MaxCount;
            prototype = Prototype;
        }

        public bool Initialize()
        {
            if (prototype)
            {
                for (int i = 0; i < maxContainerCount; ++i)
                {
                    GameObject _newInstance = Instantiate(prototype);
                    _newInstance.SetActive(false);
                    container.Add(_newInstance);
                }

                return true;
            }
            else
                throw new ArgumentNullException("ObjectPoolPayload Error", "Object Prototype is invalid or null!");
        }

        public GameObject RequestInstance()
        {
            GameObject _goNullcheck = container[containerIndex];

            if (_goNullcheck)
            {
                _goNullcheck.SetActive(true);
                containerIndex = ((containerIndex + 1) >= maxContainerCount) ? 0 : containerIndex + 1; // increment container index or reset
            }
            return _goNullcheck;
        }
    }

    public bool                                   DebugMode = true;

    private static ObjectPooler                   instance;
    private Dictionary<uint, ObjectPoolPayload>   objectPool = new Dictionary<uint, ObjectPoolPayload>();

    public static ObjectPooler GetInstance()
    {
        if (instance == null)
            instance = CreateInstance<ObjectPooler>();
        return instance;
    }

    public void RegisterObject(uint SpawnID, GameObject Object)
    {
        //  check if an object pool for the object exists
        if(objectPool.ContainsKey(SpawnID) == false)
        {
            //  if not, register object
            ObjectPoolPayload _newPayload = new ObjectPoolPayload(Object);
            _newPayload.Initialize();
            objectPool.Add(SpawnID, _newPayload);

            return;
        }

        if (DebugMode)
            Debug.Log("DEBUG: Object already registered.");
    }

    public GameObject SpawnObject(uint SpawnID)
    {
        GameObject _go = null;
        if (objectPool.ContainsKey(SpawnID))
            _go = objectPool[SpawnID].RequestInstance();
        return _go;
    }
}