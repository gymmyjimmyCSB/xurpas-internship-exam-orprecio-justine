﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GameEvent { }

public sealed class EventDispatcher //singleton
{
    private Dictionary<Event, Delegate>            eventChannels = new Dictionary<Event, Delegate>();
    private static EventDispatcher                 instance;

    public delegate void EventDelegate<T>(T e) where T : GameEvent;

    public static EventDispatcher GetInstance
    {
        get
        {
            if (instance == null)
                instance = new EventDispatcher();

            return instance;
        }
    }

    public T RaiseEventDirect<T>(T eventT, GameObject go) where T: GameEvent
    {
        return eventT;
    }

    public T RaiseEventbyChannel<T>(T eventT, int channel) where T: GameEvent
    {
        return eventT;
    } 

    public T RaiseEventGlobal<T>(T eventT) where T: GameEvent
    {
        return eventT;
    }
}