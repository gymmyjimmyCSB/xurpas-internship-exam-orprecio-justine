﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

COMBAT ACTOR SCRIPT
    * script for all game entities that is capable of combat (take damage, heal, shoot)
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventIOP : UnityEvent<int>{ } //unity event integer one param

[DisallowMultipleComponent]
public class CombatActor : Actor, ICombatant, IDamageable
{
    public bool             NeedsRigidbody = false; //  generates rigidbody on start() if needed
    public UnityEvent       OnSpawn;
    [SerializeField]
    public UnityEventIOP    OnTakeDamage;
    [SerializeField]
    public UnityEventIOP    OnHealthRecover;
    
    private ProjectileActor projectile;               // default projectile to spawn
    private Rigidbody2D     rigidbody2DRef;           // THIS gameobject's rigidbody

    public override void Reset()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);   // reset rotation
        OnSpawn.Invoke();                                  // recall OnSpawn() for any event that might "refresh" this object
    }

    public void SetProjectile(GameObject Projectile)
    {
        //  if we have any unused projectile, restore it first before replacing
        if (projectile)
            projectile.gameObject.SetActive(true);

        //  get/add projectile component on gameobject
        ProjectileActor _projectileComponent = this.GetComponentSecure<ProjectileActor>(t => { }, Projectile);
        projectile = _projectileComponent;
    }

    public void Fire(Vector3[] Trajectory)
    {
        if (projectile == null)
            return;

        // set to fire from "muzzle"
        projectile.gameObject.SetActive(true);
        projectile.transform.position = this.transform.position;
        projectile.transform.rotation = new Quaternion(0, 0, 0, 0);
        //  reset projectile physics
        projectile.rigidbody2DRef.velocity = Vector2.zero;
        projectile.rigidbody2DRef.angularVelocity = 0;
        //  add arc velocity
        projectile.TrajectoryShoot(Trajectory);
        //  empty projectile
        projectile = null;
    }

    public Rigidbody2D GetRigidbody2D()
    {
        return rigidbody2DRef;
    }

    void Awake()
    {
        OnSpawn.Invoke();
    }

    void Start()
    {
        if (NeedsRigidbody)
        {
            //  set rigidbody cache or add one
            rigidbody2DRef = this.GetComponentSecure<Rigidbody2D>(t => { }, gameObject);
        }

        OnSpawn.Invoke();
    }

    void OnEnable()
    {
        OnSpawn.Invoke();
    }

    public void TakeDamage(int Damage)
    {
        OnTakeDamage.Invoke(Damage);
    }

    public void Heal(int Recover)
    {
        OnHealthRecover.Invoke(Recover);
    }
}