﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

ACTOR CLASS
    * Base class that all game entities derive from
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;

public abstract class Actor : MonoBehaviour
{
    public uint ActorID = 0;
    public abstract void Reset();
    public uint GetID()
    {
        if (ActorID == 0)
            ActorID = (uint)Random.Range(1, 500);
        return ActorID;
    }

    public void SetID(uint ID)
    {
        ActorID = ID;
    }
}