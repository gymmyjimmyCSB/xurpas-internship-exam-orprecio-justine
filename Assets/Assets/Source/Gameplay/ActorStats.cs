﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

ACTOR STATS SCRIPT
    * All gameobjects that contains this component can take damage/heal
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[DisallowMultipleComponent]
public sealed class ActorStats : MonoBehaviour
{
    [SerializeField]
    private int         DefaultMaxHealth = 100;
    private int         currentHealth;

    public UnityEvent   OnDeath;
    public float        BonusModifier = 1.0f;   // any bonus damage to add to projectiles?

    public void Refresh()
    {
        currentHealth = DefaultMaxHealth;
        BonusModifier = 1f;
    }

    public void Heal(int HealthRecover)
    {
        if ((currentHealth + HealthRecover) > DefaultMaxHealth)
        {
            currentHealth = DefaultMaxHealth;
            return;
        }

        currentHealth += HealthRecover;
    }

    public void TakeDamage(int Damage)
    {
        if ((currentHealth - Damage) <= 0)
        {
            currentHealth = 0;
            OnDeath.Invoke();
            return;
        }

        currentHealth -= Damage;
    }

    public void DelayedDisable(float Delay)
    {
        // stop coroutine in case it's currently running
        StopCoroutine(IEDelayedDisable(Delay));
        // trigger
        StartCoroutine(IEDelayedDisable(Delay));
    }

    IEnumerator IEDelayedDisable(float Delay)
    {
        yield return new WaitForSeconds(Delay);
        gameObject.SetActive(false);
    }
}