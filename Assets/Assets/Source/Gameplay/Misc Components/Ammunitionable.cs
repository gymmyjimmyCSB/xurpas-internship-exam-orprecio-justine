﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

AMMUNITIONABLE SCRIPT
    * All gameobjects that contains this component may be loaded as player ammunition when clicked.
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;

[DisallowMultipleComponent]
public class Ammunitionable : MonoBehaviour
{
    private int damage = 10;

    void Start()
    {
        
    }

    public void SetDamage(int Damage)
    {
        damage = Damage;
    }

    void OnMouseEnter()
    {
        
    }

    void OnMouseExit()
    {
        
    }
}