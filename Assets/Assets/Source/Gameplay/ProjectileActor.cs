﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

PROJECTILE SCRIPT
    * deals damage on collision
    * projectile physics behavior
    * attached and destroyed on runtime
    * might be a good idea to implement a memory recycle
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class ProjectileActor : Actor
{
    public Rigidbody2D rigidbody2DRef
    {
        get;
        protected set;
    }

    void Awake()
    {
        rigidbody2DRef = this.GetComponentSecure<Rigidbody2D>(t => {}, gameObject);
        rigidbody2DRef.freezeRotation = true;
    }

    public void ForceShoot(Vector2 Force)
    {
        rigidbody2DRef.AddForce(Force * 50f);
    }

    public void TrajectoryShoot(Vector3[] TrajectoryPoints)
    {
        StartCoroutine(IELerpTrajectory(0.05f, TrajectoryPoints));
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (rigidbody2DRef.velocity == Vector2.zero)
            return;

        //  stop lerping trajectory if it hit something
        StopAllCoroutines();
        //  so projectile doesnt slide off after hitting ground
        rigidbody2DRef.velocity = Vector2.zero;
        //  get reference to hit health 
        ActorStats _hitActor = other.gameObject.GetComponent<ActorStats>();   

        if (_hitActor)
        {
            //  apply damage
            _hitActor.TakeDamage(50);
            // projectile object should also take damage
            gameObject.SendMessage("TakeDamage", 5.0f, SendMessageOptions.DontRequireReceiver); // would use unityevent but this script is attached and removed on runtime
            // remove projectile behaviour
            Destroy(this);
        }
    }

    IEnumerator IELerpTrajectory(float Time, Vector3[] Trajectory)
    {
        int _index = 0;
        while (_index < Trajectory.Length)
        {
            yield return new WaitForSeconds(Time);
            Vector2 _relativeDir = Trajectory[_index] - transform.position;
            rigidbody2DRef.velocity = Vector2.zero;
            rigidbody2DRef.AddRelativeForce(_relativeDir * 7, ForceMode2D.Impulse);
            _index++;
        }
    }

    public override void Reset() {} //abstract override
}