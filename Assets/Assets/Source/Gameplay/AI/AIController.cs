﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

AI CONTROLLER SCRIPT
    * contains basic move AI
-----------------------------------------------------------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

public class AIController : Controller
{
    public float MovementSpeed = 1f;

	void LateUpdate ()
    {
        transform.position -= new Vector3(MovementSpeed, 0, 0) * Time.deltaTime;
	}
}
