﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

AI DIRECTOR SCRIPT
    * decides when and what NPC to spawn in game
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

public sealed class AIDirector : MonoBehaviour
{
    public Transform SpawnPoint;
    public Actor     AISkeletonPrototype;

	// Use this for initialization
	void Start ()
    {
        if(SpawnPoint == null)
            SpawnPoint = transform; //set default value in case null

        if(AISkeletonPrototype != null)
        {
            ObjectPooler.GetInstance().RegisterObject(AISkeletonPrototype.GetID(), AISkeletonPrototype.gameObject);
            StartCoroutine(IESpawnCycle(3f, 6f, 1, SpawnPoint));
        }
	}
    
    IEnumerator IESpawnCycle(float MinDelay, float MaxDelay, uint SpawnCount, Transform SpawnPoint)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(MinDelay, MaxDelay));
            for (int i = 0; i < SpawnCount; ++i)
            {
                ObjectPooler.GetInstance().SpawnObject(AISkeletonPrototype.GetID()).transform.position = SpawnPoint.position;
            }
        }
    }	
}