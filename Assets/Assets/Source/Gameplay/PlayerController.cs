﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.

PLAYER CONTROLLER SCRIPT
    * contains a couple of default control schemes
    * PlayerController lets the current ControlScheme handle inputs from the Update() function
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using UnityEngine.Events;
using System;

public abstract class ControlScheme
{
    public abstract void HandleInput(PlayerController Controller);
    public abstract void Update();
}

public class MouseControlScheme : ControlScheme
{
    static MouseControlScheme instance;
    public static MouseControlScheme GetInstance ()
    {
        if (instance == null)
            instance = new MouseControlScheme();
        return instance;
    }

    public override void HandleInput(PlayerController Controller)
    {
        Vector2 _mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 _actortomousecenter = (_mouseposition + (Vector2)Controller.transform.position) / 2;
        _actortomousecenter += Vector2.up;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //  check if interacting with any game object
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.transform.gameObject.GetComponent<Ammunitionable>())
                {
                    hit.transform.gameObject.SetActive(false);
                    Controller.OnObjectInteract.Invoke(hit.transform.gameObject);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Controller.GetLineRenderer().enabled = true;
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            Controller.SetLineRendererTrajectory(Controller.PlotTrajectory(Controller.transform.position, _actortomousecenter, _mouseposition));
        }

        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            Controller.OnPlayerSecondaryFire.Invoke(Controller.PlotTrajectory(Controller.transform.position, _actortomousecenter, _mouseposition));
            Controller.GetLineRenderer().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Controller.SetControlScheme(KeyboardControlScheme.GetInstance());
        }
    }

    public override void Update() { throw new NotImplementedException(); }
}

public class KeyboardControlScheme : ControlScheme
{
    static KeyboardControlScheme instance;
    public static KeyboardControlScheme GetInstance()
    {
        if (instance == null)
            instance = new KeyboardControlScheme();
        return instance;
    }

    public override void HandleInput(PlayerController Controller)
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Controller.OnPlayerPrimaryFire.Invoke();
        }

        else if (Input.GetKeyDown(KeyCode.E))
        {
           // Controller.OnPlayerSecondaryFire.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Controller.SetControlScheme(MouseControlScheme.GetInstance());
        }
    }

    public override void Update() { throw new NotImplementedException(); }
}

public class Controller : MonoBehaviour { } //  derived by PlayerController and AIController
[System.Serializable]
public class UnityEventGOOP : UnityEvent<GameObject> {} // gobject 1 param unity event
[System.Serializable]
public class UnityEventV3AOP : UnityEvent<Vector3[]> { } // vector array 1 param unity event

[DisallowMultipleComponent]
public class PlayerController : Controller
{
    public UnityEvent       OnPlayerPrimaryFire;
    [SerializeField]
    public UnityEventV3AOP  OnPlayerSecondaryFire;
    [SerializeField]
    public UnityEventGOOP   OnObjectInteract;
    [SerializeField]
    public int              TrajectorySections;
    [SerializeField]
    public int              MaxTrajectoryHeight;

    private ControlScheme   currentControlScheme;
    private LineRenderer    lineRendererRef;

    public LineRenderer GetLineRenderer()
    {
        return lineRendererRef;
    }

    public void SetControlScheme(ControlScheme NewControlScheme)
    {
        if (NewControlScheme is KeyboardControlScheme)
        {
            currentControlScheme = KeyboardControlScheme.GetInstance();
        }

        else if (NewControlScheme is MouseControlScheme)
        {
            currentControlScheme = MouseControlScheme.GetInstance();
        }
    }

    void Start()
    {
        lineRendererRef = this.GetComponentSecure<LineRenderer>(t => { }, gameObject);
        lineRendererRef.SetVertexCount(TrajectorySections);

        SetControlScheme(MouseControlScheme.GetInstance());
    }

    void Update()
    {
        if(currentControlScheme != null)
            currentControlScheme.HandleInput(this);
    }

    public Vector3[] PlotTrajectory(Vector3 PointA, Vector3 PointB, Vector3 PointC)
    {
        Vector3[] _temp = new Vector3[TrajectorySections];
        float t;
        for (int i = 0; i < TrajectorySections; i++)
        {
            t = i/2.5f;
            Vector3 _quadraticCoordinates = MathHelpers.GetQuadraticCoordinates(t, PointA, PointB, PointC);
            _temp[i] = _quadraticCoordinates;
        }
        return _temp;
    }

    public void SetLineRendererTrajectory(Vector3[] Trajectory)
    {
        lineRendererRef.SetPositions(Trajectory);
    }
}