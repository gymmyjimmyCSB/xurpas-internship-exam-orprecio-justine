﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;
using System;

public static class ExtensionMethods
{
    /*  GetComponent wrapper which instantiates a component if it returns null
     *  Sample: Rigidbody2D rigidbodyRef = this.GetComponentSecure<Rigidbody2D>(t => {}, gameObject);
    */
    public static T GetComponentSecure<T>(this MonoBehaviour mono, Action<T> lambda, GameObject go) where T : Component
    {
        //  Get Component
        T t = go.GetComponent<T>();
        lambda(t);

        //  If component is null, add component
        if (t == null)
        {
            t = go.AddComponent<T>();
            lambda(t);
        }

        return t;
    }
}

public static class MathHelpers
{
    public static Vector3 GetQuadraticCoordinates(float t, Vector3 PointA, Vector3 PointB, Vector3 PointC)
    {
        return Mathf.Pow(1 - t, 2) * PointA + (((2 * t) * (1 - t)) * PointB) + Mathf.Pow(t, 2) * PointC;
    }
}