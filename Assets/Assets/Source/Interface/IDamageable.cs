﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.
-----------------------------------------------------------------------------------------------------------------------------------*/
public interface IDamageable
{
    void TakeDamage(int Damage);
    void Heal(int HealthRecover);
}