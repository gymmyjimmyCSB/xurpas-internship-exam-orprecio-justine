﻿/*-----------------------------------------------------------------------------------------------------------------------------------
-CANNONS & ARROWS-
(c) Justine Maurice Orprecio
Submitted to Xurpas, Inc. as an internship application.
-----------------------------------------------------------------------------------------------------------------------------------*/
using UnityEngine;

public interface ICombatant
{
    void Fire(Vector3[] Trajectory);
}